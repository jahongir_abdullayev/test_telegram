
import './App.css';

function App() {
  const tg = window.Telegram.WebApp
  console.log(tg)

  const onClose = () => {
    tg.close()
  }
  return (
    <div className="App">
      <div>Header</div>
      <div> {tg.initDataUnsafe?.user?.username} </div>
     <button onClick={onClose}>close</button>
    </div>
  );
}

export default App;
